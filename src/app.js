import EmblaCarousel from 'embla-carousel';
import { WheelGesturesPlugin } from 'embla-carousel-wheel-gestures';
import { startPreloadAnimation } from './preloadAnimation';

// Start the preload animation when the page loads
window.onload = () => {
    startPreloadAnimation();
};

document.addEventListener('DOMContentLoaded', function() {
    const emblaNode = document.querySelector('.embla');
    let embla;
    let options = {
        skipSnaps: true,
        dragFree: true,
        watchDrag: true, // Initially allow drag interactions
        containScroll: false
    };
    // Initialize Embla Carousel
    embla = EmblaCarousel(emblaNode, options, [WheelGesturesPlugin()]);

    // Carousel position variables
    const carousel = document.getElementById('carousel');
    const carouselTop = carousel.getBoundingClientRect().top + window.scrollY;

    // Function to update project info based on centered slide
    function updateProjectInfo() {
        const centerIndex = embla.selectedScrollSnap();
        const slides = emblaNode.querySelectorAll('.embla__slide');
        const currentSlide = slides[centerIndex];

        if (currentSlide) {
            const title = currentSlide.getAttribute('data-title');
            const year = currentSlide.getAttribute('data-year');
            const projectContentUrl = currentSlide.getAttribute('data-content');
            document.getElementById('projectTitle').textContent = title;
            document.getElementById('projectYear').textContent = year;
            loadProjectDetails(projectContentUrl);
        }
    }

    // Setup click handlers for each slide
    const slides = emblaNode.querySelectorAll('.embla__slide');
    slides.forEach((slide, index) => {
        slide.addEventListener('click', () => {
            if (slide.querySelector('img').style.opacity !== '0') {
                embla.scrollTo(index);

                // Calculate the scroll position
                const scrollCarouselTop = carouselTop -30;

                window.scroll({ top: scrollCarouselTop, behavior: 'smooth' });
            }
        });
    });

    // Update project info initially
    updateProjectInfo();

    // Listen for select events to update project info when slide changes
    embla.on('select', updateProjectInfo);

    function populateIndex() {
        const indexList = document.getElementById('indexList');
        indexList.innerHTML = ''; // Clear existing entries
    
        const chapters = document.querySelectorAll('.chapter');
        let currentBoldEntry = null; // Track the currently bold entry
    
        // Create index entries
        chapters.forEach(chapter => {
            const id = chapter.getAttribute('id');
    
            if (id) {
                const listItem = document.createElement('li');
                const indexEntry = document.createElement('a');
                indexEntry.href = `#${id}`;
                indexEntry.className = "index-entry";
                indexEntry.textContent = id.replace(/_/g, ' ').replace(/\b\w/g, char => char.toUpperCase());
                
                // Add click handler for smooth scroll
                indexEntry.addEventListener('click', (event) => {
                    event.preventDefault();
                    const targetElement = document.getElementById(id);
                    if (targetElement) {
                        const offset = 10 * window.innerHeight / 100; // 20vh offset
                        const targetPosition = targetElement.getBoundingClientRect().top + window.scrollY - offset;
                        window.scrollTo({
                            top: targetPosition,
                            behavior: 'smooth'
                        });
                    }
                });
    
                listItem.appendChild(indexEntry);
                indexList.appendChild(listItem);
            }
        });
    
        // Set up Intersection Observer
        const observerOptions = {
            root: null,
            rootMargin: '0px',
            threshold: 0.2 // Trigger when 40% of the lower end is visible
        };
    
        const observer = new IntersectionObserver((entries) => {
            entries.forEach(entry => {
                const indexEntry = document.querySelector(`a[href="#${entry.target.id}"]`);
    
                if (entry.isIntersecting) {
                    // Make the current entry bold
                    indexEntry.style.fontWeight = 'bold';
                    if (currentBoldEntry && currentBoldEntry !== indexEntry) {
                        currentBoldEntry.style.fontWeight = 'normal'; // Reset previous entry
                    }
                    currentBoldEntry = indexEntry; // Update current entry
                } else if (!entry.isIntersecting) {
                    // Reset the bold state when leaving
                    if (currentBoldEntry === indexEntry) {
                        currentBoldEntry.style.fontWeight = 'normal';
                        currentBoldEntry = null;
                    }
                }
            });
        }, observerOptions);
    
        // Observe each chapter
        chapters.forEach(chapter => {
            observer.observe(chapter);
        });
    }

    // Function to load project details content dynamically
    function loadProjectDetails(url) {
        fetch(url)
            .then(response => response.text())
            .then(data => {
                document.getElementById('project_details').innerHTML = data;

                populateIndex();
            })
            .catch(error => {
                console.error('Error loading project details:', error);
            });
    }

    // Smooth fade-out effect for .main_title on scroll
    const scrollThreshold = window.innerHeight * 0.07;
    const scrollCarouselTop = carouselTop -35;
    const mainTitle = document.querySelector('.main_title');
    const projectDetails = document.getElementById('project_details');
    const index = document.getElementById('index');

    window.addEventListener('scroll', () => {
        const scrollPosition = window.scrollY;

        // Toggle watchDrag based on scroll position
        if (scrollPosition >= scrollThreshold && options.watchDrag) {
            const currentIndex = embla.selectedScrollSnap();
            options.watchDrag = false; // Disable drag interactions
            embla.scrollTo(currentIndex); // Animate to the current centered slide
            setTimeout(() => embla.reInit(options), 800); // Reinitialize Embla Carousel with updated options after 0.5 seconds
            if (window.innerWidth >= 1440) {
                index.style.display = 'block'; // Completely remove from layout after fade-out
            }
        } else if (scrollPosition < scrollThreshold && !options.watchDrag) {
            options.watchDrag = true; // Enable drag interactions
            embla.reInit(options); // Reinitialize Embla Carousel with updated options
            if (window.innerWidth >= 1440) {
                index.style.display = 'none'; // Completely remove from layout after fade-out
            }
        }

        // Fade main title based on scroll position
        mainTitle.style.opacity = scrollPosition >= scrollThreshold ? '0' : '1';

        // Fade project details with animation
        projectDetails.style.transition = 'opacity 300ms ease-in-out';
        projectDetails.style.opacity = scrollPosition >= scrollCarouselTop ? '1' : '0';

        // Fade the index list similarly
        indexList.style.transition = 'opacity 300ms ease-in-out';
        indexList.style.opacity = scrollPosition >= scrollCarouselTop ? '1' : '0';
        
    });

    // Fade out other slides when scrolling down
    let lastScrollTop = 0;
    window.addEventListener('scroll', () => {
        const scrollPosition = window.scrollY;

        // Determine scroll direction
        const isScrollingDown = scrollPosition > lastScrollTop;
        lastScrollTop = scrollPosition;

        // Check if scroll position is beyond the threshold
        if (scrollPosition >= scrollThreshold) {
            // Fade out images of slides that are not centered
            slides.forEach((slide, index) => {
                const img = slide.querySelector('img');
                img.style.transition = 'opacity 300ms ease-in-out';
                img.style.opacity = index !== embla.selectedScrollSnap() ? '0' : '1';
            });
        } else {
            // Fade in all images when scroll position is within the threshold
            slides.forEach(slide => {
                const img = slide.querySelector('img');
                img.style.transition = 'opacity 300ms ease-in-out';
                img.style.opacity = '1';
            });
        }
    });

    /* Resume Page */

    // Open modal when clicking on resume link
    const resumeLinks = document.querySelectorAll('.resume_link a');
    const resumeModal = document.getElementById('resumeModal');
    const homeContent = document.getElementById('home'); // Select the #home element

    resumeLinks.forEach(link => {
        link.addEventListener('click', function(event) {
            event.preventDefault();

            // Ensure modal is hidden before showing (in case it's open)
            resumeModal.style.opacity = '0';
            resumeModal.style.display = 'block'; // Display the modal

            // Fade in resumeModal content after a short delay
            setTimeout(() => {
                resumeModal.style.opacity = '1'; // Fade in the modal content after displaying
                document.body.classList.add('modal-open'); // Prevent scrolling
            }, 150); // 150ms before modal fades in
        });
    });

    // Close modal when clicking on the close button
    const closeModal = document.querySelector('.close_resume a');

    closeModal.addEventListener('click', function(event) {
        event.preventDefault();

        resumeModal.style.opacity = '0'; // Start fading out modal content
        setTimeout(() => {
            resumeModal.style.display = 'none'; // Hide modal content after fade out
            document.body.classList.remove('modal-open'); // Allow scrolling
        }, 300); // Same duration as transition defined in CSS

        setTimeout(() => {
            homeContent.style.opacity = '1'; // Fade in #home content after modal is closed
        }, 150); // 150ms after modal fade out starts
    });
});