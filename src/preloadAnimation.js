export function startPreloadAnimation() {
    const preloadAnimation = document.getElementById('preloadAnimation');
    const welcomeText = document.getElementById('welcomeText');
    const jobTitleText = document.getElementById('jobTitleText');
  
    // Define your custom timing values (in ms)
    const animationDelay = 500; // Time before the animation starts
    const fadeInDuration = 1500;  // Time to fade in the text and move to center
    const visibleDuration = 500; // Time the text stays visible
    const fadeOutDuration = 500; // Time to fade out the text and move out
    const backgroundFadeOutDuration = 500; // Time for background to fade out after text
    const secondFadeInDuration = 1500; // Time to fade in the second line
    const secondVisibleDuration = 1500; // Time the second line stays visible
  
    // Set initial state for the preloadAnimation (ensure it's visible initially)
    preloadAnimation.style.transition = 'none';
    preloadAnimation.style.opacity = 1;
  
    // Set the welcomeText initial state (invisible and positioned below)
    welcomeText.style.transition = 'none'; // Start without transitions
    welcomeText.style.opacity = 0;
    welcomeText.style.transform = 'translateY(100px)'; // Start below

    // Set the jobTitleText initial state (invisible)
    jobTitleText.style.transition = 'none'; // Start without transitions
    jobTitleText.style.opacity = 0;
    jobTitleText.style.transform = 'translateY(0px)'; // Start below

    // Disable scrolling by setting overflow to hidden on the body
    document.body.style.overflow = 'hidden';
  
    // Start the animation for the text: Fade in and move to center
    setTimeout(() => {
        welcomeText.style.transition = `opacity ${fadeInDuration}ms cubic-bezier(0.16, 1, 0.3, 1), transform ${fadeInDuration}ms cubic-bezier(0.16, 1, 0.3, 1)`; // Apply custom easing for fade-in
        welcomeText.style.opacity = 1;
        welcomeText.style.transform = 'translateY(0)';
    }, animationDelay);
  
    // After fade-in duration, make the text stay visible for `visibleDuration`
    setTimeout(() => {
      welcomeText.style.transition = `opacity ${fadeOutDuration}ms, transform ${fadeOutDuration}ms`;
      welcomeText.style.opacity = 0;
      welcomeText.style.transform = 'translateY(0px)';
    }, animationDelay + fadeInDuration + visibleDuration);

    setTimeout(() => {
        jobTitleText.style.transition = `opacity ${secondFadeInDuration}ms ease`; // Apply fade-in transition
        jobTitleText.style.opacity = 1; // Fade in the second text
      },animationDelay + fadeInDuration + visibleDuration + fadeOutDuration - 300); // Start the fade-in after the first text fades out   
      
    // Keep the second text visible for a set time (secondVisibleDuration)
  setTimeout(() => {
    jobTitleText.style.transition = `opacity ${fadeOutDuration}ms ease`; // Use 'ease' for fade-out
    jobTitleText.style.opacity = 0; // Fade out the second text
  }, animationDelay + fadeInDuration + visibleDuration + fadeOutDuration + secondVisibleDuration); // Delay for the start of fade-out
  
    // Fade out the background after the text fade-out has started
    setTimeout(() => {
      preloadAnimation.style.transition = `opacity ${backgroundFadeOutDuration}ms`;
      preloadAnimation.style.opacity = 0;
      document.body.style.overflow = ''; // Restore normal scrolling
    }, animationDelay+ fadeInDuration + visibleDuration + fadeOutDuration + secondVisibleDuration + 400); // Start background fade-out after text animation
  };